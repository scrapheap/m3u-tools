# m3u Tools

A simple Perl script for generating m3u playlist from a directory.

## Requirements

As long as you have Perl v5.16+, and the core Perl modules installed then
`m3u` should run.


## Usage

To generate a m3u playlist (called `playlist.m3u`) from a directory full of
music files then use:

```bash
m3u --music-directory=<MUSIC_DIRECTORY>  >  playlist.m3u
```

I wrote this so that I can easily generate a playlist for my cheap mp3 player
that would play all the songs in album order.  My mp3 player has the layout of
a `Music` directory and a `Playlists` directory, so to create a playlist of all
the albums in the `Music` directory I do the following:

```bash
cd Playlists
m3u --music-directory=../Music > All_Albums.m3u
```

By giving the path `../Music` then the entries in the playlist are relative
paths, which seems to work well (as we don't know what the absolute path is
when the mp3 player is running).


## Non-ASCII characters

If any of your music files contain non-ASCII characters in the path/filename
then you'll get a warning and they won't be included in the playlist (this is
because my mp3 player strugles with non-ASCII filenames


## Sorting order

The order of the playlist is based on the last folder names in the paths, e.g.

```
Music/Artist_1/Album/Track_1
Music/Artist_1/Album/Track_2
Music/Artist_1/Album/Track_3
Music/Artist_1/Next_Album/Track_1
Music/Artist_1/Next_Album/Track_2
Music/Artist_1/Next_Album/Track_3
Music/Artist_2/First_Album/Track_1
Music/Artist_2/First_Album/Track_2
Music/Artist_2/First_Album/Track_3
```

Would be ordered as:

```
Music/Artist_1/Album/Track_1
Music/Artist_1/Album/Track_2
Music/Artist_1/Album/Track_3
Music/Artist_2/First_Album/Track_1
Music/Artist_2/First_Album/Track_2
Music/Artist_2/First_Album/Track_3
Music/Artist_1/Next_Album/Track_1
Music/Artist_1/Next_Album/Track_2
Music/Artist_1/Next_Album/Track_3
```

Also the words `A`, `The` and `And` are ignored for sorting if they're the
first word in the title.
